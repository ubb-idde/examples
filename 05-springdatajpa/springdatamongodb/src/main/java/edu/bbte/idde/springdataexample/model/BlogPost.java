package edu.bbte.idde.springdataexample.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.Date;

@Data
@AllArgsConstructor
@Document(collection = "BlogPost")
public class BlogPost {
    @Id
    protected ObjectId id;
    private String title;
    @DocumentReference
    private User author;
    private String content;
    private Date date;
}
