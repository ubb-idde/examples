package edu.bbte.idde.springdataexample.repo;

import edu.bbte.idde.springdataexample.model.BlogPost;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * A Spring Data MongoDB automatikusan generál implementációt ennek az interfésznek.
 */
@Repository
public interface BlogPostSpringDataDao extends MongoRepository<BlogPost, ObjectId> {
}
