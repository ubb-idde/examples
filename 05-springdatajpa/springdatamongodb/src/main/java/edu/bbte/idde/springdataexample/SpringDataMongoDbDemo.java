package edu.bbte.idde.springdataexample;

import edu.bbte.idde.springdataexample.model.BlogPost;
import edu.bbte.idde.springdataexample.model.User;
import edu.bbte.idde.springdataexample.repo.BlogPostSpringDataDao;
import edu.bbte.idde.springdataexample.repo.UserSpringDataDao;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@SpringBootApplication
@Slf4j
public class SpringDataMongoDbDemo {
    @Autowired
    private BlogPostSpringDataDao blogPostDao;

    @Autowired
    private UserSpringDataDao userDao;

    public static void main(String[] args) {
        SpringApplication.run(SpringDataMongoDbDemo.class, args);
    }

    @PostConstruct
    public void postConstruct() {
        log.info("Demoing Spring Data MongoDB");

        log.info("Removing everything from DB");
        blogPostDao.deleteAll();
        userDao.deleteAll();

        log.info("Making new entities managed, i.e. inserting");
        User user = new User(null, "Csaba", new ArrayList<>());
        user = userDao.save(user);

        BlogPost blogPost1 = new BlogPost(null, "Title 1", user, "Content 1", new Date());
        BlogPost blogPost2 = new BlogPost(null, "Title 2", user, "Longer Content 2", new Date());
        blogPost1 = blogPostDao.save(blogPost1);
        blogPost2 = blogPostDao.save(blogPost2);

        user.getBlogPosts().add(blogPost1);
        user.getBlogPosts().add(blogPost2);
        userDao.save(user);

        // összes sor lekérdezés
        log.info("Selecting all");
        Collection<BlogPost> retrievedEntities = blogPostDao.findAll();
        log.info("Retrieved entities: {}", retrievedEntities);
    }
}
