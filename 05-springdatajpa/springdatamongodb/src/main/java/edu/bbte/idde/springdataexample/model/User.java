package edu.bbte.idde.springdataexample.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.List;

@Data
@AllArgsConstructor
@Document(collection = "User")
public class User {
    @Id
    protected ObjectId id;
    private String name;

    @DocumentReference(lazy = true)
    @ToString.Exclude
    private List<BlogPost> blogPosts;
}
