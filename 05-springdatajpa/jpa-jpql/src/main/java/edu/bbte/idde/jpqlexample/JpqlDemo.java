package edu.bbte.idde.jpqlexample;

import edu.bbte.idde.jpqlexample.model.BlogPost;
import jakarta.persistence.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

public class JpqlDemo {
    public static final String AUTHOR_1 = "Author 1";
    public static final String AUTHOR_2 = "Author 2";
    private static final Logger LOG = LoggerFactory.getLogger(JpqlDemo.class);

    public static void main(String[] args) {

        // előkészület: létrehozunk pár entitást
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("blogPu");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.persist(new BlogPost("Title 1", AUTHOR_1, "Content 1", new Date()));
        entityManager.persist(new BlogPost("Title 2", AUTHOR_2, "Content 2", new Date()));
        entityManager.getTransaction().commit();

        // 1. query: findAll
        LOG.info("Selecting all");
        TypedQuery<BlogPost> query = entityManager.createQuery("from BlogPost", BlogPost.class);
        List<BlogPost> blogPosts = query.getResultList();
        LOG.info("Retrieved all entities: {}", blogPosts);
        LOG.info("");

        // 2.a query: egy oszlop lekérése
        LOG.info("Selecting IDs");
        TypedQuery<Long> idQuery = entityManager.createQuery("select id from BlogPost", Long.class);
        List<Long> ids = idQuery.getResultList();
        LOG.info("Retrieved IDs: {}", ids);
        LOG.info("");

        // 2.b query: több oszlop lekérése
        LOG.info("Selecting multiple columns");
        TypedQuery<Object[]> idAndAuthorQuery = entityManager.createQuery(
            "select id, author from BlogPost", Object[].class);
        List<Object[]> idsAndAuthors = idAndAuthorQuery.getResultList();
        for (Object[] row : idsAndAuthors) {
            LOG.info("Retrieved: id={}, author={}", row[0], row[1]);
        }
        LOG.info("");

        // 3.a. query: findByAuthor számozott paraméterrel
        LOG.info("Selecting by author with numbered parameter");
        query = entityManager.createQuery("from BlogPost where author=?1", BlogPost.class);
        query.setParameter(1, AUTHOR_1);
        blogPosts = query.getResultList();
        LOG.info("Retrieved by author numbered entities: {}", blogPosts);
        LOG.info("");

        // 3.b query: findByAuthor nevezett paraméterrel
        LOG.info("Selecting by author with named parameter");
        query = entityManager.createQuery("from BlogPost where author=:author", BlogPost.class);
        query.setParameter("author", AUTHOR_2);
        blogPosts = query.getResultList();
        LOG.info("Retrieved by author named entities: {}", blogPosts);
        LOG.info("");

        // 3.c query: findByAuthor named query-vel
        LOG.info("Selecting by author using named query");
        query = entityManager.createNamedQuery(BlogPost.FIND_BY_AUTHOR_QUERY, BlogPost.class);
        query.setParameter("author", AUTHOR_1);
        blogPosts = query.getResultList();
        LOG.info("Retrieved entities with named query: {}", blogPosts);
        LOG.info("");

        // 3.d query: findByAuthor natív query-vel
        LOG.info("Selecting by author using native query");
        Query nativeQuery = entityManager.createNativeQuery(
            "select * from db_blogpost where author=:author", BlogPost.class);
        nativeQuery.setParameter("author", AUTHOR_1);
        List<?> nativeBlogPosts = nativeQuery.getResultList();
        LOG.info("Retrieved entities with native query: {}", nativeBlogPosts);
        LOG.info("");

    }
}
