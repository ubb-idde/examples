package edu.bbte.idde.springdataexample;

import edu.bbte.idde.springdataexample.model.BlogPost;
import edu.bbte.idde.springdataexample.repo.BlogPostSpringDataDao;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collection;
import java.util.Date;

/**
 * Standard Spring Boot application.
 * Az autokonfigurációs mechanizmus detektálja a függőségek alapján a
 * konfigurálandó beaneket.
 */
@SpringBootApplication
public class SpringDataJpaDemo {
    private static final Logger LOG = LoggerFactory.getLogger(SpringDataJpaDemo.class);

    @Autowired
    private BlogPostSpringDataDao blogPostDao;

    public static void main(String[] args) {
        SpringApplication.run(SpringDataJpaDemo.class, args);
    }

    @PostConstruct
    public void postConstruct() {
        LOG.info("Demoing Spring Data");

        // új entitások létrehozása
        LOG.info("Making new entities managed, i.e. inserting");
        BlogPost entity1 = new BlogPost("Title 1", "Author 1", "Content 1", new Date());
        BlogPost entity2 = new BlogPost("Title 2", "Author 2", "Longer Content 2", new Date());
        blogPostDao.save(entity1);
        blogPostDao.save(entity2);

        // összes sor lekérdezés
        LOG.info("Selecting all");
        Collection<BlogPost> retrievedEntities = blogPostDao.findAll();
        LOG.info("Retrieved entities: {}", retrievedEntities);

        // lekérdezés ID szerint
        LOG.info("Selecting by ID");
        BlogPost retrievedEntity = blogPostDao.findById(2L).get();
        LOG.info("Retrieved entity: {}", retrievedEntity);

        // lekérdezés author szerint - generált metódus
        LOG.info("Selecting by author");
        retrievedEntities = blogPostDao.findByAuthor("Author 2");
        LOG.info("Retrieved entities: {}", retrievedEntities);

        // leghosszabb content hosszának lekérdezése - JPQL query paraméter nélkül
        LOG.info("Getting longest content length");
        Integer contentLength = blogPostDao.findLongestContentLength();
        LOG.info("Retrieved largest content length: {}", contentLength);

        // author frissítése JPQL tranzakcionális update-tel
        LOG.info("Updating authors to be the same");
        blogPostDao.updateAuthor(2L, "Author 1");
        retrievedEntities = blogPostDao.findByAuthor("Author 1");
        LOG.info("Retrieved entities after update: {}", retrievedEntities);
    }
}
