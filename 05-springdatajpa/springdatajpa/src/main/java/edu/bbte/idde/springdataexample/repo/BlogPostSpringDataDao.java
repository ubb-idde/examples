package edu.bbte.idde.springdataexample.repo;

import edu.bbte.idde.springdataexample.model.BlogPost;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * A Spring Data JPA automatikusan generál implementációt ennek az interfésznek.
 * Standard CRUD műveletek már megvannak a szülő interfészben.
 */
@Repository
public interface BlogPostSpringDataDao extends JpaRepository<BlogPost, Long> {

    /**
     * Kereső metódus, amelynek a Spring Data generál automatikusan implementációt
     * név és paraméterezés alapján
     */
    Collection<BlogPost> findByAuthor(String author);

    /**
     * Ha nem tud generálni tartalmat egy műveletnek,
     * JPQL query-t adhatunk a @Query annotáció segítségével.
     */
    @Query("select max(length(content)) from BlogPost")
    int findLongestContentLength();

    /**
     * Ha egy JPQL módosít adatokon (CUD), szükségesek a következő paraméterek:
     * - @Modifying - automatikusan executeUpdate-et használ executeQuery helyett
     * - @Transactional - a művelet elején tranzakciót nyit, a végén bezárja
     * <p>
     * A JPQL query paramétereket @Param-mal jelöljük a paraméter-listában.
     */
    @Query("update BlogPost bp set bp.author=:author where bp.id=:id")
    @Transactional
    @Modifying
    int updateAuthor(@Param("id") Long id, @Param("author") String author);
}
