package edu.bbte.idde.jpaexample;

import edu.bbte.idde.jpaexample.model.BlogPost;
import edu.bbte.idde.jpaexample.model.Comment;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class JpaRelationsDemo {
    private static final Logger LOG = LoggerFactory.getLogger(JpaRelationsDemo.class);

    public static void main(String[] args) {

        //
        // H2 in-memory adatbázis debug konzol indítása:
        // http://www.h2database.com/html/tutorial.html
        //

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("blogPu");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        // create
        entityManager.getTransaction().begin();
        BlogPost blogPost = new BlogPost("Title 1", "Author 1", "Content...", new Date());
        Comment comment = new Comment("Comment content 1");

        blogPost.getComments().add(comment);
        comment.setBlogPost(blogPost);

        entityManager.persist(blogPost);
        entityManager.getTransaction().commit();

        // retrieve all comments
        LOG.info("Comments before delete: {}", entityManager.createQuery("from Comment").getResultList());

        // delete
        entityManager.getTransaction().begin();
        entityManager.remove(blogPost);
        entityManager.getTransaction().commit();

        // retrieve all comments
        LOG.info("Comments after delete: {}", entityManager.createQuery("from Comment").getResultList());
    }
}
