package edu.bbte.idde.jpaexample;

import edu.bbte.idde.jpaexample.model.BlogPost;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class JpaHibernateDemo {
    private static final Logger LOG = LoggerFactory.getLogger(JpaHibernateDemo.class);

    public static void main(String[] args) {

        // entity manager factory készítése
        // egy persistence unithoz kapcsolódik - ez a persistence.xml-ben van definiálva
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("blogPu");

        // entity manager példány kérése a factorytól
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        // tranzakció létesítése
        entityManager.getTransaction().begin();

        // új entitások létrehozása = nem manage-elt objektumok manage-eltté tétele
        LOG.info("Making new entities managed, i.e. inserting");
        BlogPost entity1 = new BlogPost("Title 1", "Author 1", "Content 1", new Date());
        BlogPost entity2 = new BlogPost("Title 2", "Author 2", "Content 2", new Date());
        LOG.info("Entity1 before persist: {}", entity1);
        entityManager.persist(entity1);
        entityManager.persist(entity2);
        LOG.info("Entity1 after persist: {}", entity1);

        // az alábbi művelet nem csinál semmit, mivel a beadott bean már manage-elt
        entityManager.persist(entity1);
        LOG.info("Entity1 after second persist: {}", entity1);

        // tranzakció lezárása
        entityManager.getTransaction().commit();
        LOG.info("");

        // lekérdezés ID szerint
        LOG.info("Selecting by ID");
        BlogPost retrievedEntity = entityManager.find(BlogPost.class, 2L);
        LOG.info("Retrieved entity: {}", retrievedEntity);

        // nem létező ID lekérése = null
        // figyelem a logokra: itt van select, míg a fentinél nincs
        BlogPost retrievedNonExistentEntity = entityManager.find(BlogPost.class, 42L);
        LOG.info("Retrieved non-existent entity: {}", retrievedNonExistentEntity);
    }
}
