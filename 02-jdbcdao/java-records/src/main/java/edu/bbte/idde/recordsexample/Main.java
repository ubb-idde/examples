package edu.bbte.idde.recordsexample;

import com.zaxxer.hikari.HikariDataSource;
import edu.bbte.idde.recordsexample.model.BlogPost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static HikariDataSource buildDataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setJdbcUrl("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
        dataSource.setMaximumPoolSize(4);
        return dataSource;
    }

    public static void main(String[] args) throws SQLException {
        // generált adattag próba
        LOGGER.info("Using generated log attribute");

        // connection pool konfigurálása HikariCP-vel
        // kérünk kapcsolatot a poolból, s használjuk
        try (HikariDataSource dataSource = buildDataSource();
             Connection connection = dataSource.getConnection()) {
            // táblázat létrehozásának futtatása
            LOGGER.info("Creating table");
            // statement létrehozása
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate("create table BlogPost (id bigint primary key,"
                    + "author varchar(128),"
                    + "title varchar(128),"
                    + "content varchar(1024))");
                LOGGER.info("");
            }

            // beszúrunk egy sort
            String query = "insert into BlogPost values (?, ?, ?, ?)";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, 42L);
                preparedStatement.setString(2, "Title 1");
                preparedStatement.setString(3, "Author 1");
                preparedStatement.setString(4, "Content 1");
                preparedStatement.executeUpdate();
            }

            // visszakérjük a sort
            query = "SELECT id, author, title, content FROM BlogPost WHERE id = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, 42L);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    BlogPost blogPost = null;
                    if (resultSet.next()) {
                        blogPost = new BlogPost(
                            resultSet.getLong("id"),
                            resultSet.getString("author"),
                            resultSet.getString("title"),
                            resultSet.getString("content")
                        );
                    }
                    LOGGER.info("Found blog post {}", blogPost);
                }
            }
        }
    }
}
