package edu.bbte.idde.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;
import java.util.ResourceBundle;

public class I18nExample {
    private static final Logger LOG = LoggerFactory.getLogger(I18nExample.class);

    public static void main(String[] args) {
        Locale[] locales = {
            new Locale("en", "US"),
            new Locale("de", "DE"),
            new Locale("fr", "FR"),
        };

        for (Locale locale : locales) {
            LOG.info("");

            // load resource bundle
            ResourceBundle messages = ResourceBundle.getBundle("messages", locale);

            // print messages
            LOG.info("Greeting in locale {} is {}", locale, messages.getString("greeting"));
            LOG.info("Inquiry in locale {} is {}", locale, messages.getString("inquiry"));
            LOG.info("Farewell in locale {} is {}", locale, messages.getString("farewell"));
        }
    }
}
