package edu.bbte.idde.example;

import edu.bbte.idde.example.model.BlogPost;
import edu.bbte.idde.example.repo.RepositoryException;
import edu.bbte.idde.example.repo.jdbc.ConnectionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws SQLException {

        // lekérjük a connection poolt
        ConnectionPool connectionPool = ConnectionPool.getInstance();

        Connection connection = null;

        try {
            // kérünk kapcsolatot a poolból, s használjuk
            connection = connectionPool.getConnection();

            // táblázat létrehozásának futtatása
            LOG.info("Creating table");
            // statement létrehozása
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate("create table BlogPost (id bigint primary key,"
                    + "author varchar(128),"
                    + "title varchar(128),"
                    + "content varchar(1024))");
                LOG.info("");
            }

            // beszúrunk egy sort
            String query = "insert into BlogPost values (?, ?, ?, ?)";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, 42L);
                preparedStatement.setString(2, "Title 1");
                preparedStatement.setString(3, "Author 1");
                preparedStatement.setString(4, "Content 1");
                preparedStatement.executeUpdate();
            }

            // visszakérjük a sort
            query = "SELECT id, author, title, content FROM BlogPost WHERE id = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, 42L);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    BlogPost blogPost = null;
                    if (resultSet.next()) {
                        blogPost = new BlogPost();
                        blogPost.setId(resultSet.getLong(1));
                        blogPost.setAuthor(resultSet.getString(2));
                        blogPost.setTitle(resultSet.getString(3));
                        blogPost.setContent(resultSet.getString(4));
                    }
                    LOG.info("Found blog post {}", blogPost);
                }
            }
        } catch (SQLException e) {
            LOG.error("SQL execution failed", e);
            throw new RepositoryException("SQL execution failed", e);
        } finally {
            if (connection != null) {
                // visszaadjuk a connectiont
                connectionPool.returnConnection(connection);
            }
        }
    }
}
