package edu.bbte.idde.lombokexample.model;

import lombok.Data;

import java.io.Serializable;

/**
 * A @Data Java Bean generálását garantálja.
 * A generált osztály tartalmaz getter-settert, toStringet, equals/hashCode fölülírást, stb.
 */
@Data
public abstract class BaseEntity implements Serializable {
    Long id;
}
