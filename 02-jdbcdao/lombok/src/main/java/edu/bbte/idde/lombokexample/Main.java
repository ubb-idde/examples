package edu.bbte.idde.lombokexample;

import com.zaxxer.hikari.HikariDataSource;
import edu.bbte.idde.lombokexample.model.BlogPost;
import lombok.extern.slf4j.Slf4j;

import java.sql.*;

/**
 * Az @Slf4j automatikusan generál Slf4J Logger adattagot "log" név alatt
 */
@Slf4j
public class Main {
    public static HikariDataSource buildDataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setJdbcUrl("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
        dataSource.setMaximumPoolSize(4);
        return dataSource;
    }

    public static void main(String[] args) throws SQLException {
        // generált adattag próba
        log.info("Using generated log attribute");

        // connection pool konfigurálása HikariCP-vel
        // kérünk kapcsolatot a poolból, s használjuk
        try (HikariDataSource dataSource = buildDataSource();
             Connection connection = dataSource.getConnection()) {
            // táblázat létrehozásának futtatása
            log.info("Creating table");
            // statement létrehozása
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate("create table BlogPost (id bigint primary key,"
                    + "author varchar(128),"
                    + "title varchar(128),"
                    + "content varchar(1024))");
                log.info("");
            }

            // beszúrunk egy sort
            String query = "insert into BlogPost values (?, ?, ?, ?)";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, 42L);
                preparedStatement.setString(2, "Title 1");
                preparedStatement.setString(3, "Author 1");
                preparedStatement.setString(4, "Content 1");
                preparedStatement.executeUpdate();
            }

            // visszakérjük a sort
            query = "SELECT id, author, title, content FROM BlogPost WHERE id = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, 42L);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    BlogPost blogPost = null;
                    if (resultSet.next()) {
                        blogPost = new BlogPost();
                        blogPost.setId(resultSet.getLong(1));
                        blogPost.setAuthor(resultSet.getString(2));
                        blogPost.setTitle(resultSet.getString(3));
                        blogPost.setContent(resultSet.getString(4));
                    }
                    log.info("Found blog post {}", blogPost);
                }
            }
        }
    }
}
