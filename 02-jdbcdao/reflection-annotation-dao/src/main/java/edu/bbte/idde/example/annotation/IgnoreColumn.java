package edu.bbte.idde.example.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Egy oszlopra megadhatjuk, hogy
 * maradjon ki az SQL-szinkronizációs folyamatból
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface IgnoreColumn {
}