package edu.bbte.idde.example.model;

import edu.bbte.idde.example.annotation.IgnoreColumn;
import edu.bbte.idde.example.annotation.TableName;

import java.util.Date;

@TableName("BLOG_POST")
public class BlogPost extends BaseEntity {

    private String title;

    @IgnoreColumn
    private String author;

    private String content;

    private Date date;

    public BlogPost() {
        super();
    }

    public BlogPost(Long id, String title, String author, String content, Date date) {
        super(id);
        this.title = title;
        this.author = author;
        this.content = content;
        this.date = new Date(date.getTime());
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return new Date(date.getTime());
    }

    public void setDate(Date date) {
        this.date = new Date(date.getTime());
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BlogPost{");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", author='").append(author).append('\'');
        sb.append(", content='").append(content).append('\'');
        sb.append(", date=").append(date);
        sb.append('}');
        return sb.toString();
    }
}
