package edu.bbte.idde.example;

import com.zaxxer.hikari.HikariDataSource;
import edu.bbte.idde.example.model.BaseEntity;
import edu.bbte.idde.example.model.BlogPost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.Date;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class ReflectionDaoDemo {
    private static final Logger LOG = LoggerFactory.getLogger(ReflectionDaoDemo.class);

    // térképezzük a saját adattípusainkat SQL típusokra
    private static Map<Class<?>, String> TYPE_TO_SQL_TYPE;

    static {
        TYPE_TO_SQL_TYPE = new ConcurrentHashMap<>();
        TYPE_TO_SQL_TYPE.put(Integer.class, "int");
        TYPE_TO_SQL_TYPE.put(Long.class, "bigint");
        TYPE_TO_SQL_TYPE.put(String.class, "varchar(255)");
        TYPE_TO_SQL_TYPE.put(Date.class, "datetime");
    }

    /**
     * JDBC kapcsolat
     */
    HikariDataSource dataSource;

    /**
     * A modellosztály, amelyen az SQL műveleteket végezzük
     */
    Class<? extends BaseEntity> modelClass;

    /**
     * A modell osztályból generált SQL táblázat neve
     */
    String tableName;

    /**
     * A modellosztály adattagjai
     */
    List<Field> fields;

    /**
     * 0. Információk előkészítése
     */
    ReflectionDaoDemo() {
        // JDBC kapcsolat megteremtése
        dataSource = new HikariDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setJdbcUrl("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");

        // Bármely BaseEntity-t öröklő osztályt megadhatnánk itt
        modelClass = BlogPost.class;
        tableName = modelClass.getSimpleName();

        // az összes deklarált adattag - az ID nem lesz köztük, mivel szülőosztályban
        // van
        fields = Arrays.asList(modelClass.getDeclaredFields());
    }

    public static void main(String[] args) throws SQLException, InvocationTargetException, NoSuchMethodException,
        InstantiationException, IllegalAccessException {
        ReflectionDaoDemo demo = new ReflectionDaoDemo();
        demo.createTable();
        demo.insertRow();
        demo.selectRow();
    }

    /**
     * 1. Generált "create table" szkript
     */
    void createTable() throws SQLException {
        // felépítjük a "create table" szkriptet
        StringBuilder creator = new StringBuilder("create table ")
            .append(tableName)
            .append(" (");
        for (Field field : fields) {
            creator.append(field.getName())
                .append(' ')
                .append(TYPE_TO_SQL_TYPE.get(field.getType()))
                .append(", ");
        }
        creator.append("id bigint primary key auto_increment)");

        // futtatjuk a "create table"-t
        LOG.info("Executing table creation script '{}'", creator);
        try (Connection connection = dataSource.getConnection();
             Statement createStatement = connection.createStatement()) {
            createStatement.executeUpdate(creator.toString());
        }
    }

    /**
     * 2. Generált "insert" szkript
     */
    void insertRow() throws SQLException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        BlogPost model = new BlogPost(42L, "My title", "Me", "My Content", new Date());

        // "insert" parancs felépítése
        StringBuilder inserter = new StringBuilder("insert into ")
            .append(tableName)
            .append(" (");
        for (Field field : fields) {
            inserter.append(field.getName()).append(", ");
        }
        inserter.append("id) values (")
            .append(new String(new char[fields.size()]).replace("\0", "?, "))
            .append("?)");

        // PreparedStatement paraméterekkel
        LOG.info("Built insert statement '{}'", inserter);
        try (Connection connection = dataSource.getConnection();
             PreparedStatement insertStatement = connection.prepareStatement(inserter.toString())) {

            // paraméterek beállítása
            int i = 1;
            for (Field field : fields) {
                // getter metódus lekérése
                Method getter = modelClass.getDeclaredMethod("get"
                    + field.getName().substring(0, 1).toUpperCase(Locale.getDefault())
                    + field.getName().substring(1));
                // getter metódus meghívása
                Object getterResult = getter.invoke(model);
                // statement paraméter beállítása
                insertStatement.setObject(i++, getterResult);
            }
            // utoljára állítjuk az ID-t
            insertStatement.setLong(i, model.getId());

            // futtatjuk az "insert"-et
            LOG.info("Executing statement");
            insertStatement.executeUpdate();
        }
    }

    /**
     * 3. Generált "select" szkript
     */
    void selectRow() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, SQLException,
        InstantiationException {
        StringBuilder selector = new StringBuilder("select ");
        for (Field field : fields) {
            selector.append(field.getName()).append(", ");
        }
        selector.append("id from ")
            .append(tableName)
            .append(" where id=")
            .append(42L); // paraméterezhető ID, lehetne PreparedStatement

        // futtatjuk a "select"-et
        LOG.info("Executing query '{}'", selector);
        try (Connection connection = dataSource.getConnection();
             Statement selectStatement = connection.createStatement()) {
            ResultSet resultSet = selectStatement.executeQuery(selector.toString());

            if (!resultSet.next()) { // ugrunk az első visszatérített sorra
                return;
            }

            // megadott osztály új példánya - általános
            BaseEntity selectedModel = modelClass.getConstructor().newInstance();
            // ID beállítása
            selectedModel.setId(resultSet.getLong("id"));

            for (Field field : fields) {
                // result set cella lekérése név és típus alapján
                Object attribute = resultSet.getObject(field.getName(), field.getType());

                // setter metódus meghívása
                String setterName = "set"
                    + field.getName().substring(0, 1).toUpperCase(Locale.getDefault())
                    + field.getName().substring(1);
                // 1 paraméter, kötelezően ugyanolyan típusú mint a mező
                Method setter = modelClass.getDeclaredMethod(setterName, field.getType());
                // setter metódus meghívása
                setter.invoke(selectedModel, attribute);
            }

            LOG.info("Selected bean of type {}", selectedModel.getClass().getSimpleName());
            LOG.info("Bean value: {}", selectedModel);
        }
    }
}
