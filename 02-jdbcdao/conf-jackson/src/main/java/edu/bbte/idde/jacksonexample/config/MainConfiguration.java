package edu.bbte.idde.jacksonexample.config;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Általános konfigurációs bean az alkalmazásunknak
 * További modul-specifikus konfigurációs beaneket tartalmazhat
 */
public class MainConfiguration {
    // opcionális metainformáció - a JSON-ben más a név mint az adattag neve
    @JsonProperty("jdbc")
    private JdbcConfiguration jdbcConfiguration = new JdbcConfiguration();

    @JsonProperty("connectionPool")
    private ConnectionPoolConfiguration connectionPoolConfiguration = new ConnectionPoolConfiguration();

    public JdbcConfiguration getJdbcConfiguration() {
        return jdbcConfiguration;
    }

    public void setJdbcConfiguration(JdbcConfiguration jdbcConfiguration) {
        this.jdbcConfiguration = jdbcConfiguration;
    }

    public ConnectionPoolConfiguration getConnectionPoolConfiguration() {
        return connectionPoolConfiguration;
    }

    public void setConnectionPoolConfiguration(ConnectionPoolConfiguration connectionPoolConfiguration) {
        this.connectionPoolConfiguration = connectionPoolConfiguration;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("MainConfiguration{");
        sb.append("jdbcConfiguration=").append(jdbcConfiguration);
        sb.append(", connectionPoolConfiguration=").append(connectionPoolConfiguration);
        sb.append('}');
        return sb.toString();
    }
}
