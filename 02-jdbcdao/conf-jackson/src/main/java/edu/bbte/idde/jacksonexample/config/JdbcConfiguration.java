package edu.bbte.idde.jacksonexample.config;

/**
 * JDBC-specifikus konfigurációk bean variánsa
 */
public class JdbcConfiguration {
    // alapértelmezett beállítás adható meg
    // a Jackson fölülírja, ha a JSON állományban meg van adva
    private Boolean createTables = false;
    private String driverClass;
    private String url;

    public Boolean isCreateTables() {
        return createTables;
    }

    public void setCreateTables(Boolean createTables) {
        this.createTables = createTables;
    }

    public String getDriverClass() {
        return driverClass;
    }

    public void setDriverClass(String driverClass) {
        this.driverClass = driverClass;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("JdbcConfiguration{");
        sb.append("createTables=").append(createTables);
        sb.append(", driverClass='").append(driverClass).append('\'');
        sb.append(", url='").append(url).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
