package edu.bbte.idde.jacksonexample.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

public class ConfigurationFactory {
    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationFactory.class);
    private static final String CONFIG_FILE = "/application.json";

    private static MainConfiguration mainConfiguration = new MainConfiguration();

    static {
        // JSON olvasó inicializálása
        ObjectMapper objectMapper = new ObjectMapper();

        // kérünk olvasási streamet a JSON állományhoz
        try (InputStream inputStream = ConfigurationFactory.class.getResourceAsStream(CONFIG_FILE)) {
            // JSON állomány beolvasása az általunk megadott osztály egy példányába
            mainConfiguration = objectMapper.readValue(inputStream, MainConfiguration.class);
            LOG.info("Read following configuration: {}", mainConfiguration);
        } catch (IOException e) {
            LOG.error("Error loading configuration", e);
        }
    }

    public static MainConfiguration getMainConfiguration() {
        return mainConfiguration;
    }

    public static JdbcConfiguration getJdbcConfiguration() {
        return mainConfiguration.getJdbcConfiguration();
    }

    public static ConnectionPoolConfiguration getConnectionPoolConfiguration() {
        return mainConfiguration.getConnectionPoolConfiguration();
    }
}
