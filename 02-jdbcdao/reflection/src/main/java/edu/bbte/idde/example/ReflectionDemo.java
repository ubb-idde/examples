package edu.bbte.idde.example;

import java.lang.reflect.Method;

public class ReflectionDemo {
    public static void main(String[] args) throws ReflectiveOperationException {
        //
        // printer betöltése csak a typewriter interfész ismeretében
        //

        String implementationClassName = "edu.bbte.idde.example.Printer";
        Class<?> generalClass = Class.forName(implementationClassName);
        if (!Typewriter.class.isAssignableFrom(generalClass)) {
            throw new ClassCastException("The class does not inherit Typewriter correctly");
        }
        Class<Typewriter> specificClass = (Class<Typewriter>) generalClass;
        Typewriter device = specificClass.getConstructor().newInstance();
        device.typeLine("Hello");

        //
        // logger betöltése - slf4j compileOnly scope-pal
        //

        Class<?> loggerFactoryClass = Class.forName("org.slf4j.LoggerFactory");
        Method getLoggerMethod = loggerFactoryClass.getMethod("getLogger", Class.class);
        Object logger = getLoggerMethod.invoke(null, ReflectionDemo.class);

        Class<?> loggerClass = Class.forName("org.slf4j.Logger");
        Method infoMethod = loggerClass.getMethod("info", String.class);
        infoMethod.invoke(logger, "Hello, this is the logger built with reflection");
    }
}
