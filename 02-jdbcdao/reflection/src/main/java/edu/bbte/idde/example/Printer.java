package edu.bbte.idde.example;

/**
 * Megvalósítás, amit elég csak futáskor ismerni
 */
public class Printer implements Typewriter {
    @Override
    @SuppressWarnings("PMD.SystemPrintln")
    public void typeLine(String message) {
        System.out.println("Printing: " + message);
    }
}
