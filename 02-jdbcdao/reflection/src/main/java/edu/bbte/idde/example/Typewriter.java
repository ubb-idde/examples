package edu.bbte.idde.example;

/**
 * Általános interfész, amit ismerünk kompiláláskor
 */
public interface Typewriter {
    void typeLine(String message);
}
