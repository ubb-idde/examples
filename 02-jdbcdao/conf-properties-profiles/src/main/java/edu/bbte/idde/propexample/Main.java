package edu.bbte.idde.propexample;

import com.zaxxer.hikari.HikariDataSource;
import edu.bbte.idde.propexample.model.BlogPost;
import edu.bbte.idde.propexample.repo.BlogPostJdbcDao;
import edu.bbte.idde.propexample.util.PropertyProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        // adatbázis kapcsolat
        var poolSize = Integer.parseInt(PropertyProvider.getProperty("jdbc_pool_size"));
        var hikariDataSource = new HikariDataSource();
        hikariDataSource.setDriverClassName(PropertyProvider.getProperty("jdbc_driver_class"));
        hikariDataSource.setJdbcUrl(PropertyProvider.getProperty("jdbc_db_url"));
        hikariDataSource.setMaximumPoolSize(poolSize);
        LOG.info("Initialized pool of size {}", poolSize);

        var dao = new BlogPostJdbcDao(hikariDataSource);

        // beszúrunk egy sort
        LOG.info("Creating entity");
        dao.create(new BlogPost(42L, "Title 1", "Author 1", "Content 1"));

        // visszakérjük a sort
        BlogPost blogPost = dao.findById(42L);
        LOG.info("Found blog post {}", blogPost);
    }
}
