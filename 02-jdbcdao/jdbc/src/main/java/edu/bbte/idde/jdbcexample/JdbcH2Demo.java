package edu.bbte.idde.jdbcexample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class JdbcH2Demo {
    private static final Logger LOG = LoggerFactory.getLogger(JdbcH2Demo.class);

    public static void main(String[] args) throws ClassNotFoundException, SQLException {

        // driver osztály betöltése
        Class.forName("org.h2.Driver");

        // kapcsolat lekérése
        Connection connection = DriverManager.getConnection("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");

        // statement létrehozása
        Statement statement = connection.createStatement();

        // táblázat létrehozásának futtatása
        LOG.info("Creating table");
        statement.executeUpdate("create table BlogPost ("
            + "id bigint primary key auto_increment,"
            + "author varchar(128),"
            + "title varchar(128)"
            + ")");
        LOG.info("");

        // sorok beszúrása batch-csel
        LOG.info("Inserting via batch");
        statement.addBatch("insert into BlogPost values (default, 'Author 1', 'Title 1')");
        statement.addBatch("insert into BlogPost values (default, 'Author 2', 'Title 2')");
        statement.executeBatch();
        LOG.info("");

        // lekérdezés számozott indexekkel
        LOG.info("Selecting with numerical indices");
        ResultSet resultSet = statement.executeQuery("select * from BlogPost");
        while (resultSet.next()) {
            LOG.info("Found BlogPost with id={}, author='{}', title='{}'",
                resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3));
        }
        LOG.info("");

        // lekérdezés string indexekkel
        LOG.info("Selecting with string indices");
        resultSet = statement.executeQuery("select id, author, title from BlogPost");
        while (resultSet.next()) {
            LOG.info("Found BlogPost with id={}, author='{}', title='{}'",
                resultSet.getLong("id"), resultSet.getString("author"), resultSet.getString("title"));
        }
        LOG.info("");

        // parametrikus lekérdezés előkészített utasítással
        // csak egyszer szükséges előkészíteni, s véd az SQL injection ellen
        LOG.info("Selecting with parametrized prepared statement");
        PreparedStatement preparedStatement = connection.prepareStatement("select * from BlogPost where id = ?");
        preparedStatement.setLong(1, 2L);
        resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            LOG.info("Searching for id=2 yielded: id={}, author='{}', title='{}'",
                resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3));
        }
        LOG.info("");
    }
}
