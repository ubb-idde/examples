package edu.bbte.idde.jdbcexample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class JdbcH2DemoProperExceptionHandling {
    private static final Logger LOG = LoggerFactory.getLogger(JdbcH2DemoProperExceptionHandling.class);

    /**
     * Ez a metódus egyenértékű a JdbcH2Demo-val, de megfelelő kivételkezelést alkalmaz,
     * amely megnehezíti az olvashatóságot.
     * <p>
     * Minden AutoCloseable objektumot (Connection, Statement, PreparedStatement, ResultSet) try-finally
     * blokkal nyitunk és zárunk.
     */
    public static void main(String[] args) throws ClassNotFoundException {

        // driver osztály betöltése
        Class.forName("org.h2.Driver");

        // kapcsolat lekérése
        try (Connection connection = DriverManager.getConnection("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1")) {

            // statement létrehozása
            try (Statement statement = connection.createStatement()) {
                // táblázat létrehozásának futtatása
                LOG.info("Creating table");
                statement.executeUpdate("create table BlogPost ("
                    + "id bigint primary key auto_increment,"
                    + "author varchar(128),"
                    + "title varchar(128)"
                    + ")");
                LOG.info("");
            }

            // sorok beszúrása batch-csel
            LOG.info("Inserting via batch");
            try (Statement statement = connection.createStatement()) {
                statement.addBatch("insert into BlogPost values (default, 'Author 1', 'Title 1')");
                statement.addBatch("insert into BlogPost values (default, 'Author 2', 'Title 2')");
                statement.executeBatch();
                LOG.info("");
            }

            // lekérdezés számozott indexekkel
            LOG.info("Selecting with numerical indices");
            try (
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select * from BlogPost")
            ) {
                while (resultSet.next()) {
                    LOG.info("Found BlogPost with id={}, author='{}', title='{}'",
                        resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3));
                }
                LOG.info("");
            }

            // lekérdezés string indexekkel
            LOG.info("Selecting with string indices");
            try (
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select id, author, title from BlogPost")
            ) {
                while (resultSet.next()) {
                    LOG.info("Found BlogPost with id={}, author='{}', title='{}'",
                        resultSet.getLong("id"), resultSet.getString("author"), resultSet.getString("title"));
                }
                LOG.info("");
            }

            // parametrikus lekérdezés előkészített utasítással
            // csak egyszer szükséges előkészíteni, s véd az SQL injection ellen
            LOG.info("Selecting with parametrized prepared statement");
            String pquery = "select * from BlogPost where id = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(pquery)) {
                preparedStatement.setLong(1, 2L);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        LOG.info("Searching for id=2 yielded: id={}, author='{}', title='{}'",
                            resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3));
                    }
                    LOG.info("");
                }
            }

        } catch (SQLException e) {
            LOG.error("SQL exception occurred", e);
        }
    }
}
