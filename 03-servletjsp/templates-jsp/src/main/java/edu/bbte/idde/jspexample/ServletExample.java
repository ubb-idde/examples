package edu.bbte.idde.jspexample;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@WebServlet("/index")
public class ServletExample extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(ServletExample.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        LOG.info("Request arrived to example servlet");

        // kérés szintű attribútum beállítása
        req.setAttribute("setByServlet", 42);

        // továbbítás a JSP állomány felé - fogja ismerni a beállított attribútumot
        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }
}
