<!DOCTYPE html>
<html>
<head>
<title>I am a JSP page</title>
</head>
<body>

<!-- scriptlet example -->
<h1>Scriptlet</h1>
<%
// scriptlet comes here  any java code available
String hello = "hello from scriptlet";
// "out" automatically set to response writer
out.println(hello);
%>

<!-- expression example -->
<h1>Expression</h1>
<div>1+2 = <%= 1+2 %></div>
<!-- variable "hello" remembered from before -->
<div><%= hello %></div>

<h1>Accessing implicit values</h1>
<div>The value of the <code>name</code> request parameter is: <%= request.getParameter("name")%></div>
<div>The value of the <code>name</code> session variable is: <%= session.getAttribute("name")%></div>

<h1>Accessing attributes set by servlet</h1>
<!-- introduced by servlet -->
<div>Value set by servlet: <%= request.getAttribute("setByServlet") %></div>

</body>
</html>

