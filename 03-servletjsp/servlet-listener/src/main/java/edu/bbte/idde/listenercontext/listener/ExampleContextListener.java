package edu.bbte.idde.listenercontext.listener;

import edu.bbte.idde.listenercontext.service.ExampleService;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Osztály, amely figyel a kontextus indítására és lezárására,
 * vagyis arra, hogy a WAR file kitelepítődött, vagy készül a törlésre
 */
@WebListener
public class ExampleContextListener implements ServletContextListener {
    private static final Logger LOG = LoggerFactory.getLogger(ExampleContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        LOG.info("WAR deployed");
        ExampleService.getInstance().onInit();
    }

    @Override
    @SuppressWarnings("PMD.SystemPrintln")
    public void contextDestroyed(ServletContextEvent sce) {
        // VIGYÁZAT: Az slf4j logger már le van zárva ezen a ponton,
        // ezért nem fog loggolni semmit
        System.out.println("WAR is going to be deleted");
        ExampleService.getInstance().onDestroy();
    }
}
