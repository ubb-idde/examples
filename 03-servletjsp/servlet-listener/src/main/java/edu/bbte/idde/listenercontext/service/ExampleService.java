package edu.bbte.idde.listenercontext.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Egy olyan service osztály, melyen szükséges végrehajtani akciókat
 * első kitelepítéskor, s takarítást törléskor
 * <p>
 * Context listenerrel fogjuk végrehajtani ezt
 */
public final class ExampleService {
    private static final ExampleService INSTANCE = new ExampleService();
    private static final Logger LOG = LoggerFactory.getLogger(ExampleService.class);

    private ExampleService() {
    }

    public static ExampleService getInstance() {
        return INSTANCE;
    }

    /**
     * Inicializáló metódus, ami pontosan egyszer kell lefusson,
     * a WAR file kitelepítésekor.
     */
    public void onInit() {
        LOG.info("Service class initialized!");
    }

    /**
     * Feltakarítási metódus, ami pontosan egyszer kell lefusson,
     * a WAR file törlésekor.
     */
    public void onDestroy() {
        LOG.info("Service class destroying!");
    }

    public String doTheThing() {
        return "Hello!";
    }
}
