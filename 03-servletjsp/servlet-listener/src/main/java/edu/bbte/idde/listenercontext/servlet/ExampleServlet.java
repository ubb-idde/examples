package edu.bbte.idde.listenercontext.servlet;

import edu.bbte.idde.listenercontext.service.ExampleService;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@WebServlet("/example")
public class ExampleServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(ExampleServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LOG.info("GET /example");
        ExampleService exampleService = ExampleService.getInstance();
        resp.getWriter().write(exampleService.doTheThing());
    }
}
