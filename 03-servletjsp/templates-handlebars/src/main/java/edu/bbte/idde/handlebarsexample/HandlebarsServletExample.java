package edu.bbte.idde.handlebarsexample;

import com.github.jknack.handlebars.Template;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@WebServlet("/")
public class HandlebarsServletExample extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(HandlebarsServletExample.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LOG.info("Request arrived to example servlet");

        // felépítjük a dinamikus tartalmat (model)
        // itt lehetnének adatbázis-lekérések, stb.
        Map<String, Object> model = new ConcurrentHashMap<>();
        model.put("message", "I am your father");

        // rendering
        Template template = HandlebarsTemplateFactory.getTemplate("index");
        template.apply(model, resp.getWriter());
    }
}
