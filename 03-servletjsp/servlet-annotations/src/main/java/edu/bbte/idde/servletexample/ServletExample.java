package edu.bbte.idde.servletexample;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebInitParam;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ServletExample",
    initParams = {
        @WebInitParam(name = "initial", value = "42")
    },
    urlPatterns = {"/servletexample"})
public class ServletExample extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(ServletExample.class);

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        LOG.info("Initializing example servlet");
        LOG.info("Initial param: {}", config.getInitParameter("initial"));
    }

    @Override
    public void destroy() {
        super.destroy();
        LOG.info("Destroying example servlet");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        LOG.info("Request arrived to example servlet");

        PrintWriter writer = resp.getWriter();
        writer.println("Hello, I am the response body");
    }
}
