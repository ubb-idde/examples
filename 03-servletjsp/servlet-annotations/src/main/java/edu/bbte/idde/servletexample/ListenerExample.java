package edu.bbte.idde.servletexample;

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Egy listener Servlet eseményekre figyel.
 * A ServletContextListener kitelepítési eseményekre figyel.
 */
@WebListener
public class ListenerExample implements ServletContextListener {
    private static final Logger LOG = LoggerFactory.getLogger(ListenerExample.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        // ez a kód egyszer fut le minden deploy elején
        // itt lehet globális inicializálást futtatni, pl. adatbázis-kapcsolódás
        LOG.info("Initializing Servlet context on path {}", sce.getServletContext().getContextPath());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        // ez a kód egyszer fut le minden deploy leállításakor
        // itt lehet lezárni az indításkor megadottakat
        LOG.info("Destroying Servlet context on path {}", sce.getServletContext().getContextPath());
    }
}
