package edu.bbte.idde.springwebexample.controller;

import edu.bbte.idde.springwebexample.dto.incoming.BookCreationDto;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/books")
public class BookController {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createBook(@RequestBody @Valid BookCreationDto bookDto) {
        // ha ide elértünk, a body biztosan helyes
    }

}
