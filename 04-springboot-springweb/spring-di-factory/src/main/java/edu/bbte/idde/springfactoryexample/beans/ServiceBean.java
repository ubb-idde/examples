package edu.bbte.idde.springfactoryexample.beans;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Nem komponens annotációval,
 * de injektálható, mivel a ConfigurationBean factory-ként támogatja.
 */
public class ServiceBean {

    @Autowired
    private Logger log;

    public void serviceMethod() {
        log.info("I am performing a service");
    }
}
