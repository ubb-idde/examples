package edu.bbte.idde.springfactoryexample.beans;

import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClientBean {

    @Autowired
    private Logger log;

    @Autowired
    private ServiceBean serviceBean;

    @PostConstruct
    public void postConstruct() {
        log.info("In @PostConstruct of client bean");
        serviceBean.serviceMethod();
    }
}
