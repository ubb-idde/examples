package edu.bbte.idde.springwebexample.dto.outgoing;

import lombok.Data;

import java.io.Serializable;

/**
 * Egyszerű kimenő DTO
 * <p>
 * Nem tartalmaz hosszas stringeket, melyeket kollekcióban nem szeretnénk kiküldeni.
 * Kihagyható még: kapcsolódó entitás információ, stb.
 */
@Data
public class BookReducedDto implements Serializable {
    Long id;
    String title;
    String author;
    Integer releaseYear;
}
