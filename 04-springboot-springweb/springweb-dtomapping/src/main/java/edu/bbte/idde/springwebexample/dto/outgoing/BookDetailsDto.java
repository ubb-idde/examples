package edu.bbte.idde.springwebexample.dto.outgoing;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Kiterjesztett kimenő DTO.
 * Minden kimenő információ az entitásról.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class BookDetailsDto extends BookReducedDto {
    String description;
}
