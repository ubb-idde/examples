package edu.bbte.idde.springwebexample.mapper;

import edu.bbte.idde.springwebexample.dto.outgoing.BookDetailsDto;
import edu.bbte.idde.springwebexample.dto.outgoing.BookReducedDto;
import edu.bbte.idde.springwebexample.model.Book;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;

import java.util.Collection;

/**
 * Generált mapper osztály.
 * A mapstruct az alábbiak alapján generál megfelelő átalakító logikát.
 */
@Mapper(componentModel = "spring")
public abstract class BookMapper {

    @IterableMapping(elementTargetType = BookReducedDto.class)
    public abstract Collection<BookReducedDto> modelsToReducedDtos(Iterable<Book> model);

    public abstract BookDetailsDto modelToDetailsDto(Book model);
}