package edu.bbte.idde.springwebexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebDtoMappingApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringWebDtoMappingApplication.class, args);
    }
}
