package edu.bbte.idde.springwebexample.controller;

import edu.bbte.idde.springwebexample.dto.outgoing.BookDetailsDto;
import edu.bbte.idde.springwebexample.dto.outgoing.BookReducedDto;
import edu.bbte.idde.springwebexample.mapper.BookMapper;
import edu.bbte.idde.springwebexample.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/books")
public class BookController {

    @Autowired
    BookMapper bookMapper;

    @GetMapping
    public Collection<BookReducedDto> findAllBooks() {
        // Összes book megkeresése repository segítségével
        List<Book> books = List.of(
            new Book(42L, "My title", "My author", 1942, "The description is a long text")
        );

        // Átalakítás DTO-kká (nem tartalmaz leírást)
        return bookMapper.modelsToReducedDtos(books);
    }

    @GetMapping("/{bookId}")
    public BookDetailsDto findBookById(@PathVariable("bookId") Long bookId) {
        // Book megkeresése repository segítségével
        Book book = new Book(42L, "My title", "My author", 1942, "The description is a long text");

        // Átalakítás DTO-vá (tartalmaz leírást is)
        return bookMapper.modelToDetailsDto(book);
    }

}
