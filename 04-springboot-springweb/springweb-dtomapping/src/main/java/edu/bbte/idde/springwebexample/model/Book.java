package edu.bbte.idde.springwebexample.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Book implements Serializable {
    Long id;
    String title;
    String author;
    Integer releaseYear;
    String description;
}
