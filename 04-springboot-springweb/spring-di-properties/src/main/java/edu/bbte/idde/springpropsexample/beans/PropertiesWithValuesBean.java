package edu.bbte.idde.springpropsexample.beans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Konfigurációs osztály, amely tartalmazhat kintről beolvasott értékeket.
 * A @Value annotációkkal konkrét kulcsokat keres az állományban.
 */
@Configuration
@PropertySource("classpath:/config.properties")
public class PropertiesWithValuesBean {
    // properties-ből való betöltés
    @Value("${key1}")
    private String key1;

    // működik különböző típusokra is
    @Value("${key2}")
    private Integer key2;

    // nem létező kulcs esetén alapértelmezett érték megadása
    @Value("${key3:defaultValue}")
    private String key3;

    public String getKey1() {
        return key1;
    }

    public Integer getKey2() {
        return key2;
    }

    public String getKey3() {
        return key3;
    }
}
