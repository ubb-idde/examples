package edu.bbte.idde.springpropsexample.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.PropertyResolver;

/**
 * Konfigurációs osztály, amely tartalmazhat kintről beolvasott értékeket.
 * A PropertyResolver injektálásával egy Properties-szerű konténert kapunk minden értékkel.
 */
@Configuration
@PropertySource("classpath:/config.properties")
public class PropertiesWithResolverBean {

    @Autowired
    private PropertyResolver resolver;

    public PropertyResolver getResolver() {
        return resolver;
    }

    public String getProperty(String key) {
        return resolver.getProperty(key);
    }

    public String getProperty(String key, String defaultValue) {
        return resolver.getProperty(key, defaultValue);
    }

    public <T> T getProperty(String key, Class<T> targetClass) {
        return resolver.getProperty(key, targetClass);
    }

    public <T> T getProperty(String key, Class<T> targetClass, T defaultValue) {
        return resolver.getProperty(key, targetClass, defaultValue);
    }
}
