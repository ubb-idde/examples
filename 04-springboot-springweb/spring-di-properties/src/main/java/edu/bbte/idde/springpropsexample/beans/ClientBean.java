package edu.bbte.idde.springpropsexample.beans;

import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClientBean {
    private static final Logger LOG = LoggerFactory.getLogger(ClientBean.class);

    @Autowired
    private PropertiesWithValuesBean propertiesWithValues;

    @Autowired
    private PropertiesWithResolverBean propertiesWithResolver;

    @PostConstruct
    public void postConstruct() {
        LOG.info("In @PostConstruct of ClientBean");
        LOG.info("* properties via values:");
        LOG.info(" - key1 = {}", propertiesWithValues.getKey1());
        LOG.info(" - key2 = {}", propertiesWithValues.getKey2());
        LOG.info(" - key3 = {}", propertiesWithValues.getKey3());
        LOG.info("* properties via environment:");
        LOG.info(" - key1 = {}", propertiesWithResolver.getProperty("key1"));
        LOG.info(" - key2 = {}", propertiesWithResolver.getProperty("key2", Integer.class));
        LOG.info(" - key3 = {}", propertiesWithResolver.getProperty("key3", "defaultValue"));
    }
}
