package edu.bbte.idde.springwebexample.controller;

import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * HTTP kérések lekezelésére alkalmas Spring kontroller bean.
 * Minden /general-ra érkező HTTP hívás ide lesz irányitva.
 * Lehet injektálni külső függőségeket bele.
 */
@Controller
@RequestMapping("/withentities")
public class ControllerWithResponseEntities {
    private static final Logger LOG = LoggerFactory.getLogger(ControllerWithResponseEntities.class);

    @PostConstruct
    public void postConstruct() {
        LOG.info("Initializing GeneralSpringController");
    }

    /**
     * A GET kérések ide vannak irányítva.
     * ResponseEntity visszatérítéssel testre szabható a válasz (státusz, body).
     * A @RequestParam-mal kibányászhatunk urlencoded kérés-paramétereket
     */
    @GetMapping
    public ResponseEntity<String> handleGet(@RequestParam(value = "id", required = false) String id) {
        LOG.info("Handling GET request, request parameter is {}", id);
        return ResponseEntity.ok("Hello from GeneralSpringController, your id is " + id);
    }

    /**
     * A POST kérések ide vannak irányítva.
     * A @RequestBody hatására a kérés body-ja át lesz adva paraméterként.
     */
    @PostMapping
    public ResponseEntity<Void> handlePost(@RequestBody String body) {
        LOG.info("Received POST with body {}", body);
        return ResponseEntity.ok().build();
    }
}
