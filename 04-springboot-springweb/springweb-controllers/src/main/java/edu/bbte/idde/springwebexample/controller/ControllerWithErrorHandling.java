package edu.bbte.idde.springwebexample.controller;

import edu.bbte.idde.springwebexample.exception.BadRequestException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/witherrorhandling")
public class ControllerWithErrorHandling {
    /**
     * Ha az 'id' paraméter String hosszabb mint 16 karakter,
     * dobunk egy sajátos kivételt.
     * A GeneralExceptionHandler általánosan kezeli a kivételeket
     */
    @GetMapping
    @ResponseBody
    public String handleGet(@RequestParam(value = "id", required = false) String id) {
        if (id != null && id.length() > 16) {
            throw new BadRequestException("ID string is too long");
        }
        return "The provided ID is good";
    }
}
