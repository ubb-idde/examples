package edu.bbte.idde.springwebexample.controller;

import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * HTTP kérések lekezelésére alkalmas Spring kontroller bean.
 * Minden /general-ra érkező HTTP hívás ide lesz irányitva.
 * Lehet injektálni külső függőségeket bele.
 */
@Controller
@RequestMapping("/withannotations")
public class ControllerWithAnnotations {
    private static final Logger LOG = LoggerFactory.getLogger(ControllerWithAnnotations.class);

    @PostConstruct
    public void postConstruct() {
        LOG.info("Initializing ControllerWithAnnotations");
    }

    /**
     * A @ResponseBody megadásával bármi, amit visszatérít a metódus,
     * automatikusan szerializálva van a body-ba.
     * Komplex objektum esetén ez JSON formátumú lesz.
     */
    @GetMapping
    @ResponseBody
    public String handleGet(@RequestParam(value = "id", required = false) String id) {
        LOG.info("Handling GET request, request parameter is {}", id);
        return "Hello from GeneralSpringController, your id is " + id;
    }

    /**
     * A POST kérések ide vannak irányítva.
     * A @RequestStatus-ban beállított státusz tér vissza, hacsak kivétel nem lép fel.
     */
    @PostMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void handlePost(@RequestBody String body) {
        LOG.info("Received POST with body {}", body);
    }
}
