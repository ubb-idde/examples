package edu.bbte.idde.springdiexample;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
public class SpringDependencyInjectionDemo {

    /**
     * Átadjuk a control flow-t a Spring alkalmazásnak.
     * A @SpringBootApplication annotáció miatt a jelen csomagot végigszkenneli,
     * s keresi a bennük rejlő beaneket.
     */
    public static void main(String[] args) {
        SpringApplication.run(SpringDependencyInjectionDemo.class, args);
    }
}
