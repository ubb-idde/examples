package edu.bbte.idde.springdiexample.beans;

import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClientBean {
    private static final Logger LOG = LoggerFactory.getLogger(ClientBean.class);

    /**
     * Adattag-szinten megadott függőség.
     * Lehetne @Resource vagy @Inject is.
     * A konténer automatikusan megtalálja az egyetlen injektálható példányt.
     */
    @Autowired
    private ServiceInterface service;

    /**
     * Ez a metódus hívódik meg a DI konténer injektáló logikájának legvégén.
     * Minden függőségi bean garantálan be van állítva.
     */
    @PostConstruct
    public void postConstruct() {
        LOG.info("In @PostConstruct of ClientBean. Service is of type {}",
            service.getClass().getSimpleName());
        service.serviceMethod();
    }
}
