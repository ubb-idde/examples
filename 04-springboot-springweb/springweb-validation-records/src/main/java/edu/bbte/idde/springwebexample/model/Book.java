package edu.bbte.idde.springwebexample.model;

public record Book(
    Long id,
    String title,
    String author,
    Integer releaseYear,
    String description
) {
}
