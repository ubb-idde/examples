package edu.bbte.idde.springwebexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebValidationWithRecordsApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringWebValidationWithRecordsApplication.class, args);
    }
}
