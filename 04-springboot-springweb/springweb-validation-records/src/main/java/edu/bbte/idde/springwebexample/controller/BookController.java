package edu.bbte.idde.springwebexample.controller;

import edu.bbte.idde.springwebexample.dto.incoming.BookCreationDto;
import edu.bbte.idde.springwebexample.mapper.BookMapper;
import edu.bbte.idde.springwebexample.model.Book;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/books")
public class BookController {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookMapper bookMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createBook(@RequestBody @Valid BookCreationDto bookDto) {
        // ha ide elértünk, a body biztosan helyes
        LOGGER.info("Mapping {}", bookDto);
        Book book = bookMapper.creationDtoToModel(bookDto);
        LOGGER.info("Mapping result: {}", book);
    }

}
