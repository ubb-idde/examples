package edu.bbte.idde.springwebexample.mapper;

import edu.bbte.idde.springwebexample.dto.incoming.BookCreationDto;
import edu.bbte.idde.springwebexample.model.Book;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Generált mapper osztály.
 * A mapstruct az alábbiak alapján generál megfelelő átalakító logikát.
 */
@Mapper(componentModel = "spring")
public abstract class BookMapper {

    @Mapping(target = "id", ignore = true)
    public abstract Book creationDtoToModel(BookCreationDto dto);
}
