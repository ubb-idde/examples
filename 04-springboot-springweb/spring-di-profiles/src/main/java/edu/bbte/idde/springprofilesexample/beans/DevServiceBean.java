package edu.bbte.idde.springprofilesexample.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * Csak dev profilban érvényes ez a bean
 * "@Configuration"-ekre is alkalmazható.
 */
@Component
@Profile("dev")
public class DevServiceBean implements ServiceInterface {
    private static final Logger LOG = LoggerFactory.getLogger(DevServiceBean.class);

    @Override
    public void serviceMethod() {
        LOG.info("I am in development mode service bean");
    }
}
