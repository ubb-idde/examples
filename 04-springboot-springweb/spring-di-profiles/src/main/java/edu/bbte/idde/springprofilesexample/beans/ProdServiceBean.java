package edu.bbte.idde.springprofilesexample.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * Ez a bean érvényes ha NEM dev profilban vagyunk.
 */
@Component
@Profile("!dev")
public class ProdServiceBean implements ServiceInterface {
    private static final Logger LOG = LoggerFactory.getLogger(ProdServiceBean.class);

    @Override
    public void serviceMethod() {
        LOG.info("I am in production mode service bean");
    }
}
