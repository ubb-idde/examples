package edu.bbte.idde.springwebexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebDtoMappingWithRecordsApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringWebDtoMappingWithRecordsApplication.class, args);
    }
}
