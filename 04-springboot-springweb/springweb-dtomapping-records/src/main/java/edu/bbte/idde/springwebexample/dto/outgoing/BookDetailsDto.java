package edu.bbte.idde.springwebexample.dto.outgoing;

/**
 * Kiterjesztett kimenő DTO.
 * Minden kimenő információ az entitásról.
 */
public record BookDetailsDto(
    Long id,
    String title,
    String author,
    Integer releaseYear,
    String description
) {
}
