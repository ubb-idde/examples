package edu.bbte.idde.springwebexample.dto.outgoing;

/**
 * Egyszerű kimenő DTO
 * <p>
 * Nem tartalmaz hosszas stringeket, melyeket kollekcióban nem szeretnénk kiküldeni.
 * Kihagyható még: kapcsolódó entitás információ, stb.
 */
public record BookReducedDto(
    Long id,
    String title,
    String author,
    Integer releaseYear
) {
}
