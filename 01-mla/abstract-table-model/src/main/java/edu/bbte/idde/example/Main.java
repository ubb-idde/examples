package edu.bbte.idde.example;

import edu.bbte.idde.example.presentation.BlogPostTableModel;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        JTable table = new JTable();
        table.setModel(new BlogPostTableModel());

        JPanel panel = new JPanel();
        panel.add(new JScrollPane(table));

        JFrame frame = new JFrame();
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
}
