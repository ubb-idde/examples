package edu.bbte.idde.example.presentation;

import edu.bbte.idde.example.model.BlogPost;

import javax.swing.table.AbstractTableModel;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("PMD.SystemPrintln")
public class BlogPostTableModel extends AbstractTableModel {
    private static final String[] COLUMN_NAMES = {"ID", "Title", "Author", "Content", "Date"};

    private final List<BlogPost> blogPosts;

    public BlogPostTableModel() {
        super();

        // adatok
        // jobb lenne, ha egy fölsőbb réteg alapján kapnánk meg
        this.blogPosts = new ArrayList<>();
        this.blogPosts.add(
            new BlogPost(1L, "title 1", "author 1", "content 1", Instant.now())
        );
        this.blogPosts.add(
            new BlogPost(2L, "title 2", "author 2", "content 2", Instant.now().plus(Duration.ofMinutes(30L)))
        );
    }

    @Override
    public int getRowCount() {
        return this.blogPosts.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int column) {
        return COLUMN_NAMES[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        var blogPost = blogPosts.get(rowIndex);
        return switch (columnIndex) {
            case 0 -> blogPost.getId();
            case 1 -> blogPost.getTitle();
            case 2 -> blogPost.getAuthor();
            case 3 -> blogPost.getContent();
            case 4 -> blogPost.getDate();
            default -> null;
        };
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        // ID-n kívül minden módosítható
        return columnIndex > 0;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        var blogPost = blogPosts.get(rowIndex);
        switch (columnIndex) {
            case 1 -> blogPost.setTitle("" + value);
            case 2 -> blogPost.setAuthor("" + value);
            case 3 -> blogPost.setContent("" + value);
            case 4 -> blogPost.setDate(Instant.parse("" + value));
            default -> {
            }
        }

        System.out.println(blogPosts);
    }
}
