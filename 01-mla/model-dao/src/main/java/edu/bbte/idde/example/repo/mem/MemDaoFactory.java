package edu.bbte.idde.example.repo.mem;

import edu.bbte.idde.example.repo.BlogPostDao;
import edu.bbte.idde.example.repo.DaoFactory;

/**
 * Az absztract DAO factory egyik konkretizációja.
 * Garantálja, hogy mindegyik entitás elérése
 * egy ugyanazon módszerrel történik.
 */
public class MemDaoFactory extends DaoFactory {

    @Override
    public BlogPostDao getBlogPostDao() {

        // mindig új példányt térítünk vissza
        // itt lehetne implementálni a
        // singleton vagy pooling mintákat is
        return new BlogPostMemDao();
    }
}
