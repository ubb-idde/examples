package edu.bbte.idde.example.repo;

import edu.bbte.idde.example.model.BaseEntity;

import java.util.Collection;

/**
 * CRUD műveleteket absztraktan meghatározó interfész.
 * Azok a műveletek kerülnek ide, melyek minden entitásban szükségesen
 * támogatottak.
 * <p>
 * Generikus bármilyen modellosztályra, mely örökli a BaseEntity-t.
 */
public interface Dao<T extends BaseEntity> {

    /**
     * Teljes entitáslista visszatérítése.
     */
    Collection<T> findAll();

    /**
     * Létrehozás entitás által
     */
    void create(T entity);

    // további CRUD műveletek...
}
