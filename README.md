# IDDE laborfeladat példák

- Az előadás különböző tematikáihoz tartozó példaprogramok elérhetőek jelen publikus git tárolón.
- A példaprogramok tematika szerint vannak csoportosítva. Minden tematikának egy külön könyvtár felel meg, ezen belül megtalálhatóak az aktuális példaprogramok.

### Első letöltés

- git *konzol*parancs:
    - navigáljunk azon mappába, ahova letöltenénk a példákat: `cd <celpont>`
    - letöltés: `git clone https://gitlab.com/bbte-mmi/idde/examples` (létrehoz egy `examples` almappát)
- *VS Code*-ból:
    - Command pallette: `Ctrl+Shift+P`
    - *"Git: Clone"*
    - Repo URL: `https://gitlab.com/bbte-mmi/idde/examples`

### A tartalom frissítése

- git *konzol*parancs:
    - navigáljunk azon mappába, ahova korábban letöltöttük a példákat: `cd <celpont>/examples`
    - frissítés: `git pull`
- *VS Code*-ból:
![](vscode-sync.png){width="40%"}

### Hiba esetén

Hiba léphet fel, hogyha a lokális tárolóban levő tartalom *megváltozott* (ha kézzel változtatunk a példaprogramok tartalmán). A könyvtár *tisztításáért* hívjuk a következőt ugyanazon `examples` mappából (**Vigyázat!:** Ezen parancsok meghívása a laborfeladatos tárolóban kitörölheti annak módosításait):
```
    git reset --hard HEAD
    git clean -xfd
```
